from django.test import TestCase
from .views import Counter
from django.test import Client
# create an instance of the client for our use
client = Client()

# Create your tests here.
class CounterTests(TestCase):
    def test_increment(self):
        counter = Counter()
        counter.increment()
        incremented = counter.count
        self.assertEqual(incremented, 1)

    def test_counter_multi_increment(self):
        counter = Counter()

        counter.increment()
        counter.increment()
        assert counter.count == 2

    def test_counter_separate_instances(self):
        counter1 = Counter()
        counter2 = Counter()

        counter1.increment()
        assert counter1.count == 1
        assert counter2.count == 0

    def test_init(self):
        counter = Counter()
        self.assertEqual(counter.count, 0)


class PageViewTests(TestCase):
    def test_get_nonexistent_page(self):
        response = self.client.get('/page/fake')
        self.assertEqual(response.status_code, 404)

    def test_post_updates_page(self):
        response = self.client.post('/page/test', {'content': 'new content'})
        self.assertEqual(response.status_code, 200)

    def test_post_creates_new_page(self):
        response = self.client.post('/page/new', {'content': 'new page content'})
        self.assertEqual(response.status_code, 200)

    def test_get_existing_page(self):
        response = self.client.get('/page/index.html')
        self.assertEqual(response.status_code, 200)
