"""items URL Configuration
"""

from django.urls import path
from . import views, tests

urlpatterns = [
    path('', views.index, name='index'),
    path('<name>', views.page, name='page'),
    path('page/index.html', views.index, name='index'),
    path('page/<name>', views.page, name='page2'),
]
